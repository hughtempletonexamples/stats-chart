"use strict";

var utils = require("../testUtils");

describe("homeService", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var scope;
  var homeService;
  var projectsRef;
  var referenceDataRef;
  var mockProjectsService;
  var mockReferenceDataService;
  
  var project1 = {
    "name": "Project 1",
    "id": "001",
    "active": true,
    "estimatedAreas": {
      "Ext": 200,
      "Floor": 10,
      "Int": 100,
      "Roof": 400
    },
    "gateway": "G4",
    "siteStart": "2018-04-06",
    "datumType": "Education Student Accommodation"
  };

  var project2 = {
    "name": "Project 2",
    "id": "002",
    "active": true,
    "estimatedAreas": {
      "Ext": 200,
      "Floor": 10,
      "Int": 100,
      "Roof": 400
    },
    "gateway": "G4",
    "siteStart": "2018-03-27",
    "datumType": "Education Student Accommodation",
    "weekDuration": 2
  };

  var project3 = {
    "name": "Project 3",
    "id": "003",
    "active": true,
    "estimatedAreas": {
      "Ext": 200,
      "Floor": 10,
      "Int": 100,
      "Roof": 400
    },
    "gateway": "G5",
    "siteStart": "2018-04-03",
    "datumType": "Education Student Accommodation",
    "weekDuration": 4
  };

  var project4 = {
    "name": "Project 4",
    "id": "004",
    "active": true,
    "estimatedAreas": {
      "Ext": 200,
      "Floor": 10,
      "Int": 100,
      "Roof": 400
    },
    "gateway": "G5",
    "siteStart": "2018-04-04",
    "datumType": "Education Student Accommodation",
    "weekDuration": 2
  };

  var project5 = {
    "name": "Project 5",
    "id": "005",
    "active": true,
    "estimatedAreas": {
      "Ext": 200,
      "Floor": 10,
      "Int": 100,
      "Roof": 400
    },
    "gateway": "G5",
    "siteStart": "2018-04-05",
    "datumType": "Education Student Accommodation",
    "weekDuration": 2
  };

  var project6 = {
    "name": "Project 6",
    "id": "006",
    "active": true,
    "estimatedAreas": {
      "Ext": 200,
      "Floor": 10,
      "Int": 100,
      "Roof": 400
    },
    "gateway": "G6",
    "siteStart": "2018-04-30",
    "datumType": "Education Student Accommodation",
    "weekDuration": 4
  };

  beforeEach(angular.mock.module("statsChart.home", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  // Angular mock our projects module
  beforeEach(angular.mock.module("statsChart.home"));

  beforeEach(function () {
    jasmine.clock().install();
    jasmine.clock().mockDate(new Date("2018-05-22"));
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });
  
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    referenceDataRef = firebaseRoot.child("srd");
    
    utils.addQuerySupport(projectsRef);
    utils.addQuerySupport(referenceDataRef);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes", "getProductionLines", "getGateways"]);

  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductionLines.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productionLines"));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getGateways.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("gateways"));
    });
  }));

  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1,
      "PROJ2": project2,
      "PROJ3": project3,
      "PROJ4": project4,
      "PROJ5": project5,
      "PROJ6": project6
    });

    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "Floor": { productionLine: "CASS" }
      }
    );

    referenceDataRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 40 },
        "HSIP": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 60 },
        "CASS": { defaultCapacity: 6 }
      }
    );
  
    referenceDataRef.child("gateways").set(
      {
        "G1": {
          "name": "G1 – Discover",
          "colour": "#ffff80",
          "seq": 0
        },
        "G2": {
          "name": "G2 – Consider",
          "colour": "#b30000",
          "seq": 1
        },
        "G3": {
          "name": "G3 – Decide",
          "colour": "#ff1a1a",
          "seq": 2
        },
        "G4": {
          "name": "G4 – Tender",
          "colour": "#ff8080",
          "seq": 3
        },
        "G5": {
          "name": "G5 – Pre-Order",
          "colour": "#2d5986",
          "seq": 4
        },
        "G6": {
          "name": "G6 – Delivery Strategy",
          "colour": "#538cc6",
          "seq": 5
        },
        "G7": {
          "name": "G7 - Pre-Construction",
          "colour": "#9fdf9f",
          "seq": 6
        },
        "G8": {
          "name": "G8 – Site Start",
          "colour": "#53c653",
          "seq": 7
        },
        "G9": {
          "name": "G9 – Site Handover",
          "colour": "#4f9943",
          "seq": 8
        },
        "G10": {
          "name": "G10 – Feedback",
          "colour": "#ff80df",
          "seq": 9
        }
      }
    );

  });
  
  beforeEach(inject(function ($rootScope, _homeService_) {
    scope = $rootScope;
    homeService = _homeService_;
  }));
  
  describe("getData test chartData", function () {

    it("returns forecast data", function () {

      homeService.getData(true, 5, 0)
        .then(function (data) {

          expect(data.chartData.SIP).toEqual(
            [
              {
                key: "G10",
                color: "#ff80df",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G9",
                color: "#4f9943",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G8",
                color: "#53c653",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G7",
                color: "#9fdf9f",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G6",
                color: "#538cc6",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 50]]
              },
              {
                key: "G5",
                color: "#2d5986",
                values: [["2018-03-26", 0], ["2018-04-02", 250], ["2018-04-09", 250], ["2018-04-16", 50], ["2018-04-23", 50], ["2018-04-30", 0]]
              },
              {
                key: "G4",
                color: "#ff8080",
                values: [["2018-03-26", 100], ["2018-04-02", 116.66666666666667], ["2018-04-09", 16.666666666666668], ["2018-04-16", 16.666666666666668], ["2018-04-23", 16.666666666666668], ["2018-04-30", 16.666666666666668]]
              },
              {
                key: "G3",
                color: "#ff1a1a",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G2",
                color: "#b30000",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G1",
                color: "#ffff80",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              }
            ]);

          expect(data.chartData.TF).toEqual(
            [
              {
                key: "G10",
                color: "#ff80df",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G9",
                color: "#4f9943",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G8",
                color: "#53c653",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G7",
                color: "#9fdf9f",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G6",
                color: "#538cc6",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 25]]
              },
              {
                key: "G5",
                color: "#2d5986",
                values: [["2018-03-26", 0], ["2018-04-02", 125], ["2018-04-09", 125], ["2018-04-16", 25], ["2018-04-23", 25], ["2018-04-30", 0]]
              },
              {
                key: "G4",
                color: "#ff8080",
                values: [["2018-03-26", 50], ["2018-04-02", 58.333333333333336], ["2018-04-09", 8.333333333333334], ["2018-04-16", 8.333333333333334], ["2018-04-23", 8.333333333333334], ["2018-04-30", 8.333333333333334]]
              },
              {
                key: "G3",
                color: "#ff1a1a",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G2",
                color: "#b30000",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G1",
                color: "#ffff80",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              }
            ]);

          expect(data.chartData.CASS).toEqual(
            [
              {
                key: "G10",
                color: "#ff80df",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G9",
                color: "#4f9943",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G8",
                color: "#53c653",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G7",
                color: "#9fdf9f",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G6",
                color: "#538cc6",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 102.5]]
              },
              {
                key: "G5",
                color: "#2d5986",
                values: [["2018-03-26", 0], ["2018-04-02", 512.5], ["2018-04-09", 512.5], ["2018-04-16", 102.5], ["2018-04-23", 102.5], ["2018-04-30", 0]]
              },
              {
                key: "G4",
                color: "#ff8080",
                values: [["2018-03-26", 205], ["2018-04-02", 239.16666666666669], ["2018-04-09", 34.16666666666667], ["2018-04-16", 34.16666666666667], ["2018-04-23", 34.16666666666667], ["2018-04-30", 34.16666666666667]]
              },
              {
                key: "G3",
                color: "#ff1a1a",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G2",
                color: "#b30000",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              },
              {
                key: "G1",
                color: "#ffff80",
                values: [["2018-03-26", 0], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
              }
            ]);

        });

      scope.$apply();

    });



    it("tests for duration of 1 week", function () {

      var project1 = {
        "name": "Project 1",
        "id": "001",
        "active": true,
        "estimatedAreas": {
          "Ext": 200,
          "Floor": 10,
          "Int": 100,
          "Roof": 400
        },
        "gateway": "G10",
        "weekDuration": 1,
        "siteStart": "2018-03-27",
        "datumType": "Education Student Accommodation"
      };

      projectsRef.set({
        "PROJ1": project1
      });

      homeService.getData(true, 5, 0)
        .then(function (data) {
          expect(data.chartData.SIP[0]).toEqual({
            key: "G10",
            color: "#ff80df",
            values: [["2018-03-26", 200], ["2018-04-02", 0], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
          });
        });
      scope.$apply();
    });

    it("tests for duration of 2 weeks and division", function () {

      var project1 = {
        "name": "Project 1",
        "id": "001",
        "active": true,
        "estimatedAreas": {
          "Ext": 200,
          "Floor": 10,
          "Int": 100,
          "Roof": 400
        },
        "gateway": "G10",
        "weekDuration": 2,
        "siteStart": "2018-03-27",
        "datumType": "Education Student Accommodation"
      };

      projectsRef.set({
        "PROJ1": project1
      });

      homeService.getData(true, 5, 0)
        .then(function (data) {
          expect(data.chartData.SIP[0]).toEqual({
            key: "G10",
            color: "#ff80df",
            values: [["2018-03-26", 100], ["2018-04-02", 100], ["2018-04-09", 0], ["2018-04-16", 0], ["2018-04-23", 0], ["2018-04-30", 0]]
          });
        });
      scope.$apply();
    });

  });
  
  describe("getData test dates", function () {
    
    it("tests for weekly dates", function () {
      
      homeService.getData(true, 5, 0)
        .then(function (data) {
          expect(data.monthData.SIP).toEqual({
            G10: {"2018-03": 0, "2018-04": 0},
            G9: {"2018-03": 0, "2018-04": 0},
            G8: {"2018-03": 0, "2018-04": 0},
            G7: {"2018-03": 0, "2018-04": 0},
            G6: {"2018-03": 0, "2018-04": 50},
            G5: {"2018-03": 0, "2018-04": 600},
            G4: {"2018-03": 100, "2018-04": 183.33333333333334},
            G3: {"2018-03": 0, "2018-04": 0},
            G2: {"2018-03": 0, "2018-04": 0},
            G1: {"2018-03": 0, "2018-04": 0}
          });
          
          expect(data.monthData.TF).toEqual({
            G10: {"2018-03": 0, "2018-04": 0},
            G9: {"2018-03": 0, "2018-04": 0},
            G8: {"2018-03": 0, "2018-04": 0},
            G7: {"2018-03": 0, "2018-04": 0},
            G6: {"2018-03": 0, "2018-04": 25},
            G5: {"2018-03": 0, "2018-04": 300},
            G4: {"2018-03": 50, "2018-04": 91.66666666666667},
            G3: {"2018-03": 0, "2018-04": 0},
            G2: {"2018-03": 0, "2018-04": 0},
            G1: {"2018-03": 0, "2018-04": 0}
          });
          
          expect(data.monthData.CASS).toEqual({
            G10: {"2018-03": 0, "2018-04": 0},
            G9: {"2018-03": 0, "2018-04": 0},
            G8: {"2018-03": 0, "2018-04": 0},
            G7: {"2018-03": 0, "2018-04": 0},
            G6: {"2018-03": 0, "2018-04": 102.5},
            G5: {"2018-03": 0, "2018-04": 1230},
            G4: {"2018-03": 205, "2018-04": 375.83333333333337},
            G3: {"2018-03": 0, "2018-04": 0},
            G2: {"2018-03": 0, "2018-04": 0},
            G1: {"2018-03": 0, "2018-04": 0}
          });
          
        });
      scope.$apply();
    });
  
  });
 
  describe("getData test dates", function () {
    
    it("tests for weekly dates", function () {
      
      homeService.getData(true, 5, 0)
        .then(function (data) {
          expect(data.monthData.SIP).toEqual({
            G10: {"2018-03": 0, "2018-04": 0},
            G9: {"2018-03": 0, "2018-04": 0},
            G8: {"2018-03": 0, "2018-04": 0},
            G7: {"2018-03": 0, "2018-04": 0},
            G6: {"2018-03": 0, "2018-04": 50},
            G5: {"2018-03": 0, "2018-04": 600},
            G4: {"2018-03": 100, "2018-04": 183.33333333333334},
            G3: {"2018-03": 0, "2018-04": 0},
            G2: {"2018-03": 0, "2018-04": 0},
            G1: {"2018-03": 0, "2018-04": 0}
          });
          
          expect(data.monthData.TF).toEqual({
            G10: {"2018-03": 0, "2018-04": 0},
            G9: {"2018-03": 0, "2018-04": 0},
            G8: {"2018-03": 0, "2018-04": 0},
            G7: {"2018-03": 0, "2018-04": 0},
            G6: {"2018-03": 0, "2018-04": 25},
            G5: {"2018-03": 0, "2018-04": 300},
            G4: {"2018-03": 50, "2018-04": 91.66666666666667},
            G3: {"2018-03": 0, "2018-04": 0},
            G2: {"2018-03": 0, "2018-04": 0},
            G1: {"2018-03": 0, "2018-04": 0}
          });
          
          expect(data.monthData.CASS).toEqual({
            G10: {"2018-03": 0, "2018-04": 0},
            G9: {"2018-03": 0, "2018-04": 0},
            G8: {"2018-03": 0, "2018-04": 0},
            G7: {"2018-03": 0, "2018-04": 0},
            G6: {"2018-03": 0, "2018-04": 102.5},
            G5: {"2018-03": 0, "2018-04": 1230},
            G4: {"2018-03": 205, "2018-04": 375.83333333333337},
            G3: {"2018-03": 0, "2018-04": 0},
            G2: {"2018-03": 0, "2018-04": 0},
            G1: {"2018-03": 0, "2018-04": 0}
          });
          
        });
      scope.$apply();
    });
  
  });

  describe("getData test projectsByGateway", function () {

    it("get all projects for weeks", function () {

      homeService.getData(true, 5, 0)
        .then(function (data) {

          expect(data.projectsByGateway.SIP).toEqual({
            G10: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G9: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G8: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G7: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G6: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": ["006"]},
            G5: {"2018-03-26": [], "2018-04-02": ["003","004","005"], "2018-04-09": ["003","004","005"], "2018-04-16": ["003"], "2018-04-23": ["003"], "2018-04-30": []},
            G4: {"2018-03-26": ["002"], "2018-04-02": ["001","002"], "2018-04-09": ["001"], "2018-04-16": ["001"], "2018-04-23": ["001"], "2018-04-30": ["001"]},
            G3: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G2: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G1: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []}
          });
          expect(data.projectsByGateway.TF).toEqual({
            G10: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G9: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G8: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G7: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G6: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": ["006"]},
            G5: {"2018-03-26": [], "2018-04-02": ["003","004","005"], "2018-04-09": ["003","004","005"], "2018-04-16": ["003"], "2018-04-23": ["003"], "2018-04-30": []},
            G4: {"2018-03-26": ["002"], "2018-04-02": ["001","002"], "2018-04-09": ["001"], "2018-04-16": ["001"], "2018-04-23": ["001"], "2018-04-30": ["001"]},
            G3: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G2: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G1: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []}
          });
          expect(data.projectsByGateway.CASS).toEqual({
            G10: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G9: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G8: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G7: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G6: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": ["006"]},
            G5: {"2018-03-26": [], "2018-04-02": ["003","004","005"], "2018-04-09": ["003","004","005"], "2018-04-16": ["003"], "2018-04-23": ["003"], "2018-04-30": []},
            G4: {"2018-03-26": ["002"], "2018-04-02": ["001","002"], "2018-04-09": ["001"], "2018-04-16": ["001"], "2018-04-23": ["001"], "2018-04-30": ["001"]},
            G3: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G2: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []},
            G1: {"2018-03-26": [], "2018-04-02": [], "2018-04-09": [], "2018-04-16": [], "2018-04-23": [], "2018-04-30": []}
          });
        });
      scope.$apply();
    });
 
  });
  
  describe("getData test monthProjectsByGateway", function () {
    
    it("get all projects for months", function () {
      
      homeService.getData(true, 5, 0)
        .then(function (data) {
          expect(data.monthProjectsByGateway.SIP).toEqual({
            G10: {"2018-03": [], "2018-04": []},
            G9: {"2018-03": [], "2018-04": []},
            G8: {"2018-03": [], "2018-04": []},
            G7: {"2018-03": [], "2018-04": []},
            G6: {"2018-03": [], "2018-04": ["006"]},
            G5: {"2018-03": [], "2018-04": ["003","004","005"]},
            G4: {"2018-03": ["002"], "2018-04": ["001","002"]},
            G3: {"2018-03": [], "2018-04": []},
            G2: {"2018-03": [], "2018-04": []},
            G1: {"2018-03": [], "2018-04": []}
          });
          expect(data.monthProjectsByGateway.TF).toEqual({
            G10: {"2018-03": [], "2018-04": []},
            G9: {"2018-03": [], "2018-04": []},
            G8: {"2018-03": [], "2018-04": []},
            G7: {"2018-03": [], "2018-04": []},
            G6: {"2018-03": [], "2018-04": ["006"]},
            G5: {"2018-03": [], "2018-04": ["003","004","005"]},
            G4: {"2018-03": ["002"], "2018-04": ["001","002"]},
            G3: {"2018-03": [], "2018-04": []},
            G2: {"2018-03": [], "2018-04": []},
            G1: {"2018-03": [], "2018-04": []}
          });
          expect(data.monthProjectsByGateway.CASS).toEqual({
            G10: {"2018-03": [], "2018-04": []},
            G9: {"2018-03": [], "2018-04": []},
            G8: {"2018-03": [], "2018-04": []},
            G7: {"2018-03": [], "2018-04": []},
            G6: {"2018-03": [], "2018-04": ["006"]},
            G5: {"2018-03": [], "2018-04": ["003","004","005"]},
            G4: {"2018-03": ["002"], "2018-04": ["001","002"]},
            G3: {"2018-03": [], "2018-04": []},
            G2: {"2018-03": [], "2018-04": []},
            G1: {"2018-03": [], "2018-04": []}
          });
        });
      scope.$apply();
    });
  
  });
  
});
