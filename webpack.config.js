"use strict";

var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require("clean-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var Webpack = require('webpack');
var path = require("path");

var mainEntryPoint = path.resolve(__dirname, "app", "configured_app.js");
var publicDirectory = path.resolve(__dirname, "public");
var distDirectory = path.resolve(__dirname, "dist");

module.exports = {
  entry: {
    "statsChart-main": mainEntryPoint
  },
  plugins: [
    new Webpack.DefinePlugin({
      FIREBASE: "\"" + process.env.statsChart_FIREBASE + "\"",
      FIREBASEAPIKEY: "\"" + process.env.statsChart_FIREBASE_APIKEY + "\""
    }),
    
    new ExtractTextPlugin("stylesheets/[name].css", {allChunks: true}),

    new Webpack.optimize.CommonsChunkPlugin({ name: "statsChart-common", filename: "statsChart-common.js", minChunks: 2}),

    new CleanWebpackPlugin(["dist"], {
      root: path.resolve(__dirname),
      verbose: true,
      dry: false
    }),
    
  ],
  output: {
    path: distDirectory,
    filename: "[name].js"
  },
  devServer: {
    contentBase: publicDirectory,
    port : 8080
  },
  devtool: "source-map",
  module: {
    loaders: [
      {
        test: /\.s?css$/,
        loader: ExtractTextPlugin.extract("style", "css?-url!sass")
      },
      {
        test: /\.html$/,
        loader: "file"
      },
      {
        test: /\.jpg$/,
        loader: "file"
      },
      { // for third-party minified scripts, don't process require()
        test: /\/xlsx\//,
        include: /(node_modules|bower_components)/,
        loader: "script"
      }
    ]
  },
  noInfo: true
};