(function () {
  "use strict";

  angular
    .module("statsChart.home")
    .factory("homeService", homeService);

  /**
   * @ngInject
   */
  function homeService($log, $q, schema, dateUtil, firebaseUtil) {
    
    var today = new Date();
    today.setDate(1);
    var prevMonth = today.setMonth(today.getMonth()-1);
    prevMonth = dateUtil.weekStart(prevMonth);
    
    var service = {
      getData: getData
    };

    return service;

    ////////////

    function getData(isTest, weeksAmount, dayzGone) {

      var mainData = {};
      var chartData = {};
      var monthData = {};
      var projectsByGateway = {};
      var monthProjectsByGateway = {};
      var gatewayData = {};
      var productTypeProductionLine = {};
      var returnData = {};
      var monthsAmount = weeksAmount / 4;
      var defaultDuration = 12;

      var deferred = $q.defer();
      
      var gateways = getGateways();
      var productTypes = getProductTypes();
      var productionLines = getProductionLines();
      
      firebaseUtil.loaded(gateways, productTypes, productionLines)
        .then(buildData)
        .then(function () {
          return getProjects().$loaded();
        })
        .then(prepareData)
        .then(modelForChart)
        .then(function () {
          returnData["chartData"] = chartData;
          returnData["monthData"] = monthData;
          returnData["projectsByGateway"] = projectsByGateway;
          returnData["monthProjectsByGateway"] = monthProjectsByGateway;
          deferred.resolve(returnData);
        });

      return deferred.promise;
      
      function buildData() {
        gateways.sort(compareGateways);
        gateways.reverse();
        gatewayData = buildMap(gateways, "colour");
        productTypeProductionLine = buildMap(productTypes, "productionLine");
        angular.forEach(productionLines, function (productionLine) {
          var productionLineKey = productionLine.$id;
          if (angular.isUndefined(mainData[productionLineKey])) {
            mainData[productionLineKey] = {};
          }
          if (angular.isUndefined(monthData[productionLineKey])) {
            monthData[productionLineKey] = {};
          }
          if (angular.isUndefined(projectsByGateway[productionLineKey])) {
            projectsByGateway[productionLineKey] = {};
          }
          if (angular.isUndefined(monthProjectsByGateway[productionLineKey])) {
            monthProjectsByGateway[productionLineKey] = {};
          }
          angular.forEach(gateways, function (gateway) {
            var gatewayKey = gateway.$id;
            if (angular.isUndefined(mainData[productionLineKey][gatewayKey])) {
              mainData[productionLineKey][gatewayKey] = {};
            }
            if (angular.isUndefined(monthData[productionLineKey][gatewayKey])) {
              monthData[productionLineKey][gatewayKey] = {};
            }
            if (angular.isUndefined(projectsByGateway[productionLineKey][gatewayKey])) {
              projectsByGateway[productionLineKey][gatewayKey] = {};
            }
            if (angular.isUndefined(monthProjectsByGateway[productionLineKey][gatewayKey])) {
              monthProjectsByGateway[productionLineKey][gatewayKey] = {};
            }
            var weeks = getWeeks(weeksAmount, dayzGone);
            angular.forEach(weeks, function (week) {
              var siteStart = new Date(week);
              siteStart = dateUtil.weekStart(siteStart);
              if (angular.isUndefined(projectsByGateway[productionLineKey][gatewayKey][siteStart])) {
                projectsByGateway[productionLineKey][gatewayKey][siteStart] = [];
              }
              if (angular.isUndefined(mainData[productionLineKey][gatewayKey][siteStart])) {
                mainData[productionLineKey][gatewayKey][siteStart] = 0;
              }
            });
            var months = buildMonths(monthsAmount);
            angular.forEach(months, function (month) {
              var siteStart = new Date(month);
              siteStart = dateUtil.dateMonth(siteStart);
              if (angular.isUndefined(monthProjectsByGateway[productionLineKey][gatewayKey][siteStart])) {
                monthProjectsByGateway[productionLineKey][gatewayKey][siteStart] = [];
              }
              if (angular.isUndefined(monthData[productionLineKey][gatewayKey][siteStart])) {
                monthData[productionLineKey][gatewayKey][siteStart] = 0;
              }
            });
          });
        });
      }

      function prepareData(projects) {
        var durationOfWeeks;
        var duration;
        angular.forEach(projects, function (project) {
          if (angular.isDefined(project.siteStart)) {
            var siteStart = new Date(project.siteStart);
            var week = dateUtil.weekStart(siteStart);
            if (angular.isDefined(project.weekDuration)){
              durationOfWeeks = buildDuration(week, project.weekDuration);
              duration = project.weekDuration;
            }else{
              durationOfWeeks = buildDuration(week, defaultDuration);
              duration = defaultDuration;
            }
            for (var i = 0; i<= durationOfWeeks.length; i++){
              var weekBetween = durationOfWeeks[i];
              var weekDate = new Date(weekBetween);
              var month = dateUtil.dateMonth(weekDate);
              angular.forEach(project.estimatedAreas, function (meterSquared, type) {
                var productionLine = productTypeProductionLine[type];
                if (angular.isDefined(project.gateway)) {
                  if (angular.isDefined(projectsByGateway[productionLine][project.gateway][weekBetween])) {
                    if(projectsByGateway[productionLine][project.gateway][weekBetween].indexOf(project.id) === -1){
                      projectsByGateway[productionLine][project.gateway][weekBetween].push(project.id);
                    }
                  }
                  if (angular.isDefined(mainData[productionLine][project.gateway][weekBetween])) {
                    mainData[productionLine][project.gateway][weekBetween] += meterSquared / duration;
                  }
                  if (angular.isDefined(monthProjectsByGateway[productionLine][project.gateway][month])) {
                    if(monthProjectsByGateway[productionLine][project.gateway][month].indexOf(project.id) === -1){
                      monthProjectsByGateway[productionLine][project.gateway][month].push(project.id);
                    }
                  }
                  if (angular.isDefined(monthData[productionLine][project.gateway][month])) {
                    monthData[productionLine][project.gateway][month] += meterSquared / duration;
                  }
                }
              });
            } 
          }
        });
      }  
      
      function modelForChart(){
        angular.forEach(mainData, function (theData, productionLine) {
          if (angular.isUndefined(chartData[productionLine])){
            chartData[productionLine] = [];
          }
          angular.forEach(theData, function (forecastDatums, gateway) {
            var dataOb = {
              "key": gateway,
              "color": gatewayData[gateway],
              "values": []
            };
            angular.forEach(forecastDatums, function (forecastData, week) {
              var value = [];
              var monthDate = new Date(week);
              if (isTest){
                value.push(week);
              }else{
                value.push(monthDate.getTime());
              }
              value.push(forecastData);
              dataOb.values.push(value);
            });
            chartData[productionLine].push(dataOb);
          });
          chartData[productionLine].sort(compareChartGateways);
          chartData[productionLine].reverse();
        });
      }
      
      function compareChartGateways(a, b) {
        a = a.key.replace("G","");
        b = b.key.replace("G","");
        return a - b;
      }
      
      function compareGateways(a, b) {
        a = a.$id.replace("G","");
        b = b.$id.replace("G","");
        return a - b;
      }
      
      function buildMap(items, property) {
        var map = {};
        angular.forEach(items, function (item) {
          map[item.$id] = item[property];
        });
        return map;
      }
      
      function buildDuration(siteStart, duration){
        var buildWeeks = [];
        buildWeeks.push(siteStart);
        for (var j = 1; j < duration; j++) {
          siteStart = dateUtil.plusDays(siteStart, 7);
          buildWeeks.push(siteStart);
        }
        return buildWeeks;
      }
      
      function getWeeks(weeksAmount, dayzGone) {
        var weekHolder = [];
        var monday = dateUtil.weekStart(prevMonth);
        var firstMonday = dateUtil.minusDays(monday, dayzGone);
        weekHolder.push(firstMonday);
        for (var j = 1; j <= weeksAmount; j++) {
          firstMonday = dateUtil.plusDays(firstMonday, 7);
          weekHolder.push(firstMonday);
        }
        return weekHolder;
      }
      
      function buildMonths(monthsAmount) {
        var months = [];
        var tmpDate;

        for (var i = 0; i < monthsAmount; i++) {
          tmpDate = new Date(prevMonth);
          tmpDate.setDate(1);
          tmpDate.setMonth(tmpDate.getMonth() + i);
          tmpDate = dateUtil.dateMonth(tmpDate);
          months.push(tmpDate);
        }
        return months;
      }
      
      function getProjects() {
        return schema.getArray(schema.getRoot().child("projects").orderByChild("id"));
      }

      function getProductionLines() {
        return getReferenceData("productionLines");
      }

      function getProductTypes() {
        return getReferenceData("productTypes");
      }

      function getGateways() {
        return getReferenceData("gateways");
      }

      function getReferenceData(key) {
        var srdRef = schema
          .getRoot()
          .child("srd")
          .child(key);
        return schema.getArray(srdRef);
      }

    }
  }
  
})();
