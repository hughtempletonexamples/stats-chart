(function () {
  "use strict";

  angular
    .module("statsChart.home", [
      "ngRoute",
      "firebase",
      "statsChart.core"
    ]);

})();
