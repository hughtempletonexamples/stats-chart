(function () {
  "use strict";

  angular
    .module("statsChart.home")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/home", {
        templateUrl: require("./home.html"),
        controller: "HomeController",
        controllerAs: "vm"
      });
  }


})();
