(function () {
  "use strict";

  angular.module("statsChart", [
    "ngRoute",
    "ngAnimate",
    "ngTouch",
    "ngMessages",
    "ui.bootstrap",
    "nvd3",
    
    "statsChart.core",
    "statsChart.home"
  ]);
})();
