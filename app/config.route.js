(function () {
  "use strict";

  angular
    .module("statsChart")
    .config(config);

  /**
   * @ngInject
   */
  function config($routeProvider) {
    $routeProvider.otherwise({redirectTo: "/home"});
  }
})();
