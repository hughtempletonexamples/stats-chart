(function () {
  "use strict";

  angular.module(
    "statsChart.core",
    [
      "firebase.database",
      "firebase.auth"
    ]);
})();
