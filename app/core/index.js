(function () {
  "use strict";

  require("expose?firebase!firebase");
  require("angularfire");

  require("./module");
  require("./firebase-util");
  require("./date-util");
  require("./schema.service");

})();
