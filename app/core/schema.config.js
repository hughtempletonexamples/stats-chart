/* global PRODUCTION:false FIREBASE:false FIREBASEAPIKEY:false */
(function () {
  "use strict";
  angular
    .module("statsChart.core")
    .run(function ($window, $location) {

      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyA8bCy6-Ni8ZEBK9GhZ3htmJFXh10FPka4",
        authDomain: "stat-chart.firebaseapp.com",
        databaseURL: "https://stat-chart.firebaseio.com",
        projectId: "stat-chart",
        storageBucket: "stat-chart.appspot.com",
        messagingSenderId: "248549607778"
      };

      // Initialize Firebase connection
      $window.firebase.initializeApp(config);
    });
})();

