(function () {
  "use strict";

  angular
    .module("statsChart.core")
    .provider("dateUtil", dateUtil);

  /**
   * @ngInject
   */
  function dateUtil() {
    this.$get = function () {
      return {
        daysBetween: daysBetween,
        daysBetweenWeekDaysOnly: daysBetweenWeekDaysOnly,
        plusDays: plusDays,
        minusDays: minusDays,
        subtractWorkingDays: subtractWorkingDays,
        isAfter: isAfter,
        isWeekDay: isWeekDay,
        dayOfWeek: dayOfWeek, // Mon => 0, ..., Sun => 6
        weekStart: weekStart,
        dateMonth: dateMonth,
        todayAsIso: todayAsIso,
        toIsoDate: toIsoDate,
        toUserDate: toUserDate,
        toUserDateNew: toUserDateNew,
        asDate: asDate,
        validDate: validDate
      };
    };

    ////////////

    function daysBetween(from, to) {
      var toDate = asDate(to);
      var fromDate = asDate(from);
      var millis = toDate.getTime() - fromDate.getTime();
      return Math.round(millis / 1000 / 24 / 60 / 60);
    }
    
    function daysBetweenWeekDaysOnly(from, to) {
      var toDate = asDate(to);
      var fromDate = asDate(from);
      var minusWeekends = 0;
      if (fromDate < toDate){
        while(fromDate < toDate){
          if (isWeekDay(fromDate)){
            minusWeekends++;
          }
          fromDate = plusDays(fromDate, 1);
          fromDate = asDate(fromDate);
        }
      } else if (fromDate > toDate){
        while(fromDate > toDate){
          if (isWeekDay(fromDate)){
            minusWeekends--;
          }
          fromDate = minusDays(fromDate, 1);
          fromDate = asDate(fromDate);
        }
      }
      return minusWeekends;
    }

    function plusDays(value, days) {
      var date = new Date(value);
      date.setDate(date.getDate() + days);
      return toIsoDate(date);
    }
    
    function minusDays(value, days) {
      var date = new Date(value);
      date.setDate(date.getDate() - days);
      return toIsoDate(date);
    }

    function subtractWorkingDays(startDate, days) {
      var date = asDate(startDate);

      while (days > 0) {
        date.setDate(date.getDate()-1);
        if (isWeekDay(date)) {
          days--;
        }
      }
      return toIsoDate(date);
    }

    function isAfter(dateA, dateB) {
      return asDate(dateA) > asDate(dateB);
    }

    function isWeekDay(value) {
      var day = asDate(value).getDay();
      return day >= 1 && day <= 5;
    }

    function dayOfWeek(value) {
      var date = asDate(value);
      return (date.getDay() + 6) % 7;
    }
    
    function dateMonth(value) {
      var date = asDate(value);
      return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2);
    }

    function weekStart(value) {
      if (angular.isDefined(value)) {
        var date = new Date(value);
      } else {
        var date = new Date();
      }
      date.setDate(date.getDate() - dayOfWeek(date));

      return toIsoDate(date);
    }

    function asDate(value) {
      return angular.isDate(value) ? value : new Date(value);
    }

    function toIsoDate(value) {
      var date = asDate(value);
      return date.getFullYear()+"-" + ("0" + (date.getMonth()+1)).slice(-2) + "-"+ ("0" + date.getDate()).slice(-2);
    }
    
    function toUserDate(value) {
      var date = asDate(value);
      return ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
    }
    
    function toUserDateNew(value) {
      var date = asDate(value);
      return ("0" + date.getDate()).slice(-2) + "/" + ("" + (date.getMonth()+1)).slice(-2) + "/" + date.getFullYear();
    }

    function todayAsIso() {
      return toIsoDate(new Date());
    }
    
    function validDate(date) {
      date = asDate(date);
      var isDate = false;
      if (angular.isDate(date)) {
        isDate = true;
        if (isNaN(date.getTime())) {  // d.valueOf() could also work
          isDate = false;
        } else {
          isDate = true;
        }
      } else {
        isDate = false;
      }
      return isDate;
    }
  }

})();
